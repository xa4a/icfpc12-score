ICFPC 2012 unofficial scoreboard client and server.

This is running live at [http://o0o.org.ua/icfpc](http://o0o.org.ua/icfpc)

Usage:

    # Download test runner (about 2.5M, contains lots of maps)
    wget https://bitbucket.org/xa4a/icfpc12-score/downloads/icfpc12-score-client.tar.gz

    # Unpack.
    tar xzvf icfpc12-score-client.tar.gz

    # Verify nothing in icfpc12-score/ steals your passwords

    # Run submitter and follow onscreen instructions to enter your team name and
    # ./lifter location.
    # Note: this gives every map 150 seconds to solve, so will take a while.
    # You may want to leave this in a screen(1) and go outside for a walk.
    icfpc12-score/submit_scores.sh
