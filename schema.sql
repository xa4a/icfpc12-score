CREATE TABLE IF NOT EXISTS scores (
    score_id INTEGER PRIMARY_KEY,
    team_name VARCHAR(512),
    map_name VARCHAR(64),
    score INTEGER,
    date TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);
CREATE INDEX IF NOT EXISTS team_name_idx ON scores (team_name);
CREATE INDEX IF NOT EXISTS map_name ON scores (map_name);
