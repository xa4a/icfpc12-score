#!/bin/bash

which timeout >/dev/null
if [ $? != 0 ]; then
  echo "timeout command not found. exiting."
  exit 1
fi
which curl >/dev/null
if [ $? != 0 ]; then
  echo "curl command not found. exiting."
  exit 1
fi

MAP_PATTERN=$1
if [ "$MAP_PATTERN"z == "z" ]; then
    MAP_PATTERN='*'
fi

echo "Enter team name:"
read team_name
echo "Enter path to ./lifter:"
read lifter

if [ ! -f $lifter ]; then
  echo "$lifter: lifter not found"
  exit 1
fi

trap "echo Killing..; kill -9 $$" SIGINT SIGTERM


MY_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
LIFTER_DIR="$( cd "$( dirname ${lifter} )" && pwd)"
SUBMIT_URL="http://o0o.org.ua/icfpc/"


tmp_file="$(mktemp /tmp/icfpc.tmpXXX)"

if [ $(uname) == "Darwin" ]; then
  timeout="timeout -2 150"
else
  timeout="timeout -s 2 -k 10 150"
fi

echo "Watch results at ${SUBMIT_URL}"


cd ${LIFTER_DIR}
for map in `find ${MY_DIR}/maps/ -type f -name "${MAP_PATTERN}"`; do
  map=${map//\/\//\/}
  $timeout ./lifter < ${map} > $tmp_file 2> /dev/null
  solution=$(cat $tmp_file)
  map_name=${map##${MY_DIR}/}
  echo ${map_name}:
  score=$(${MY_DIR}/src/simulator.py ${map} $solution)
  if [ $? != 0 ]; then
    echo "No solution"
  else
    echo $score
    curl -X POST -d '{"team_name": "'${team_name}'", "map_name": "'${map_name}'", "score": '$score'}' $SUBMIT_URL >/dev/null 2>&1
  fi
done
