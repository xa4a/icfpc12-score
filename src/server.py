import logging
import os
import threading
import Queue
import sqlite3
from jinja2 import Environment, FileSystemLoader

import tornado.ioloop
import tornado.web
import tornado.escape

logging.basicConfig(level=logging.INFO)
log = logging.getLogger(__name__)
DB_NAME = 'scores.dat'
SCHEMA_FILE = 'schema.sql'
INSERT_Q = "INSERT INTO scores (team_name, map_name, score) VALUES (?, ?, ?);"

class InvalidDataException(Exception):
    pass

def clean_data(json_text):
    """Converts incoming json object into a dict to be put in the DB.

    Args:
        json_text: string json representation with string fields team_name,
            map_name and integer score.
    Returns:
        a dict, similar to the one, expected in args
    """

    try:
        data = tornado.escape.json_decode(json_text)
        res = {'team_name': str(data['team_name']),
               'map_name': str(data['map_name']),
               'score': int(data['score'])}
        return res
    except (IndexError, TypeError, ValueError) as e:
        raise InvalidDataException(str(e))

class DbReader(object):
    def __init__(self, db_name):
        def dict_factory(cursor, row):
            d = {}
            for idx, col in enumerate(cursor.description):
                d[col[0]] = row[idx]
            return d
        self.db = sqlite3.connect(db_name)
        schema = open(SCHEMA_FILE).read()
        log.info('Executing %s', schema)
        self.db.executescript(schema)
        self.db.commit()

        self.db.row_factory = dict_factory

    def return_result(self, query, args=[]):
        log.info("Executing %s %s", query, args)
        cur = self.db.cursor()
        cur.execute(query, args)
        return cur.fetchall()

    def scores(self, team_name=None, map_name=None):
        query = "SELECT team_name, map_name, score FROM scores"
        where = []
        args = []
        if team_name is not None:
            where.append('team_name = ?')
            args.append(team_name)
        if map_name is not None:
            where.append('map_name = ?')
            args.append(map_name)
        if where:
            query += ' WHERE ' + ' AND '.join(where)

        query += ' ORDER BY map_name ASC, score DESC'

        return self.return_result(query, args)

    def all_maps(self):
        query = "SELECT DISTINCT map_name FROM scores ORDER BY map_name"
        return self.return_result(query)

    def all_teams(self):
        query = "SELECT DISTINCT team_name FROM scores ORDER BY team_name"
        return self.return_result(query)


class DbWriter(threading.Thread):
    db = None

    def __init__(self, db_name):
        self.db_name = db_name
        self.q = Queue.Queue()
        super(DbWriter, self).__init__()

    def _init_db(self):
        self.db = sqlite3.connect(self.db_name)
        schema = open(SCHEMA_FILE).read()
        log.info('Executing %s', schema)
        self.db.executescript(schema)
        self.db.commit()

    def write(self, item):
        self.q.put(item)

    def run(self):
        while True:
            item = self.q.get()
            self._actually_write(item)
            self.q.task_done()

    def _actually_write(self, item):
        if self.db is None:
            self._init_db()
        c = self.db.cursor()
        log.info('Inserting: %s', item)
        c.execute(INSERT_Q,
                  (item['team_name'], item['map_name'], item['score']))
        self.db.commit()


class MainHandler(tornado.web.RequestHandler):
    def initialize(self, db_reader, db_writer, jinja_env):
        self.db_reader = db_reader
        self.db_writer = db_writer
        self.jinja_env = jinja_env

    def get(self):
        map_name = self.get_argument('map_name', None)
        team_name = self.get_argument('team_name', None)
        scores = self.db_reader.scores(team_name=team_name, map_name=map_name)
        teams = self.db_reader.all_teams()
        maps = self.db_reader.all_maps()
        t = self.jinja_env.get_template('index.html')

        map_txt = ''
        if map_name:
          maps_root = os.path.abspath(
              os.path.join(os.path.dirname(__file__), '..', 'maps'))
          map_file = os.path.join(maps_root, map_name[len('maps/'):])

          map_path = os.path.abspath(map_file)
          if map_path.startswith(maps_root):
            try:
              map_txt = open(map_path).read()
            except IOError:
              pass
        self.write(t.render(scores=scores, maps=maps, teams=teams,
                            map_name=map_name, team_name=team_name,
                            map_txt=map_txt))

    def post(self):
        try:
            data = clean_data(self.request.body)
            self.db_writer.write(data)
            self.set_status(200)
            self.finish('OK\n')
        except InvalidDataException:
            logging.exception('Bad request: %s', self.request.body)
            self.send_error(400)
        except Exception:
            logging.exception("Something terrible happened!")



if __name__ == "__main__":
    db_writer = DbWriter(DB_NAME)
    db_writer.daemon = True
    db_writer.start()
    db_reader = DbReader(DB_NAME)

    jinja_env = Environment(loader=FileSystemLoader('templates'),
                            autoescape=True)

    settings = {
        "static_path": os.path.join(os.path.abspath(os.path.dirname(__file__)), '..', 'static'),
    }

    application = tornado.web.Application([
        (r"/", MainHandler, dict(db_writer=db_writer, db_reader=db_reader,
                                 jinja_env=jinja_env)),
        (r"/static/(.*)", tornado.web.StaticFileHandler,
         dict(path=settings['static_path'])),
    ])

    application.listen(8888)
    tornado.ioloop.IOLoop.instance().start()
